package com.example.user.recycler.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.user.recycler.message_models.MessageModels;
import com.example.user.recycler.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by OLEG on 7/6/17.
 *
 * This adapter sended in a TAG in TextView ID of selected receiver
 */

public class DialogsListAdapter extends RecyclerView.Adapter<DialogsListAdapter.ViewHolder> {

    private ArrayList<MessageModels>uniqueSenders;
    private View.OnClickListener onClickListener;


    public DialogsListAdapter(ArrayList<MessageModels>unique, View.OnClickListener onClickListener) {

        this.uniqueSenders = unique;
        this.onClickListener = onClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dialog, parent, false);
        return new ViewHolder(v, onClickListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
     holder.mTextView.setText(uniqueSenders.get(position).getFrom());
        //TODO setting tag to textview
        holder.mTextView.setTag(uniqueSenders.get(position).getFrom());

        if (!uniqueSenders.get(position).isOnline()){
            holder.isOnlineView.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return uniqueSenders.size();
    }

    /**
     * Holder for DialogListAdapter
     *
     * mTextView - pecepient's name
     * mImageview - CircleImageView, pecepient's photo
     * isOnline - textview visible, if user online
     * mCardView - CardView for items, clickable
     *
     */
    class ViewHolder extends RecyclerView.ViewHolder{

        final TextView mTextView;
        final CircleImageView mImageView;
        final TextView isOnlineView;
        final CardView mCardView;

        public ViewHolder(View itemView,
                          View.OnClickListener onClickListener) {
            super(itemView);
            mTextView = itemView.findViewById(R.id.diaview);
            mImageView = itemView.findViewById(R.id.dia_ava);
            isOnlineView = itemView.findViewById(R.id.isonline);
            mCardView = itemView.findViewById(R.id.dia_card);
            mCardView.setClickable(true);
            mCardView.setOnClickListener(onClickListener);
        }
    }
}
