package com.example.user.recycler.message_models;

import com.example.user.recycler.message_models.MessageModels;
import com.example.user.recycler.message_models.MessagesSingleton;
import com.example.user.recycler.services.CurrentUser;

import java.util.ArrayList;

/**
 * Created by OLEG on 7/6/17.
 * Класс для парсинга сообщений
 *
 * Принимает массив сообщений из синглета MessagesSingleton
 *
 * Поля:
 *      ArrayList<String> uniqueId - массив строк с уникальными id собеседников, аргумент для DialogListAdapter
 *      ArrayList<MessageModels> messages
 *
 */

public class MessagesParsed  {

    ArrayList<MessageModels> uniqueId;
    ArrayList<MessageModels> messages;
    private ArrayList<String> mId;

    /**
     * Конструктор инициализирует uniqueId и запускает метод createDataForDialoglist, без параметров
     */

    //region Constructor
    public MessagesParsed() {
        this.uniqueId = new ArrayList<>();
        this.mId = new ArrayList<>();
        createDataForDialogList();

    }
    //endregion


    //region Getter
    public ArrayList<MessageModels> getUniqueId() {
        return uniqueId;
    }
    //endregion


    /**
     *  This method receives arrayList of MessageModels objects from singleton and parse
     *  this. If mId are not contain value of field "from" every MessageModels objects,
     *  this value is added in mId and message added in uniqueId
     */
    private void createDataForDialogList(){
        messages = MessagesSingleton.getInstance().models;
        for (int i = 0; i<messages.size(); i++) {
            if (!mId.contains(messages.get(i).getFrom())){
                mId.add(messages.get(i).getFrom());
                uniqueId.add(messages.get(i));
            }
        }
    }

    /**
     * This method receives String from - id recepient of current messages
     * and chooses messages from singleton
     * @param from - id recepient of messages
     * @return ArrayList<MessagesModels> with Messages for current recepient
     */

    public ArrayList<MessageModels> createDataForSingleDialog(String from){

        ArrayList<MessageModels> messages = new ArrayList<>();
        this.messages = MessagesSingleton.getInstance().models;
        for (int i = 0; i < this.messages.size() ; i++) {

            if (this.messages.get(i).getFrom().equals(from)&& this.messages.get(i).getTo().equals(CurrentUser.mId)||
                    this.messages.get(i).getTo().equals(from)&& this.messages.get(i).getFrom().equals(CurrentUser.mId)){
                messages.add(this.messages.get(i));
            }
        }return messages;
    }
}
