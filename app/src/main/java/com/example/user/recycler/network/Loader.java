package com.example.user.recycler.network;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Created by OLEG on 7/13/17.
 */

public class Loader  {

    public Thread t;
    StringBuilder stringBuilder;
    private String mHTML;
    static String link;


    public Loader(String link) {
        Loader.link = link;
        uploadHTML();
    }

    public void uploadHTML(){

        t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(Loader.link);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));

                    stringBuilder = new StringBuilder();
                    String line = reader.readLine();
                    while (!line.equals("</html>")){
                        stringBuilder.append(line+"\n");
                        line = reader.readLine();
                        //System.out.println(line);
                    }


                } catch (IOException e) {
                    Log.i("TAG", "cant upload page");
                }mHTML =  stringBuilder.toString();
            }
        });
        t.start();


    }

    public String getHTML() throws InterruptedException {
        t.join();
        return mHTML;
    }


}