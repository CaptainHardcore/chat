package com.example.user.recycler.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;

import com.example.user.recycler.message_models.MessagesParsed;
import com.example.user.recycler.R;
import com.example.user.recycler.message_models.MessageModels;
import com.example.user.recycler.adapters.AdapterRec;
import com.example.user.recycler.network.HTMLParser;
import com.example.user.recycler.services.CurrentUser;
import com.example.user.recycler.services.Switcher;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;

import static android.app.Activity.RESULT_OK;
import static android.util.Patterns.WEB_URL;


/**
 * Created by user on 7/5/17.
 */

public class MainFrafment extends Fragment {

    private RecyclerView mRecyclerView;
    private AdapterRec mAdapterRec;
    private ImageButton mImageButton;
    private EmojiconEditText mCompleteTextView;
    public static String from;
    private ImageButton mBackButton;
    private TextView mToolbarTextView;
    private ArrayList<MessageModels> dataset;
    private MessagesParsed mParsedMessages;
    private ImageButton mAddFilesButton;
    private ImageView mImageView;
    private ImageView isPickedView;
    private static final int PICK_IMAGE = 1;
    private boolean ispicked;
    private ImageButton smileButton;
    public static   HTMLParser parser;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        //region Uploading Data
        mParsedMessages = new MessagesParsed();

        dataset = mParsedMessages.createDataForSingleDialog(Switcher.ID);
        //endregion


        //region Toolbar Settings
        Toolbar mToolbar = view.findViewById(R.id.toolbar);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(mToolbar);
        mToolbar.setTitle("");
        mToolbarTextView = view.findViewById(R.id.username_toolbar);
        mToolbarTextView.setText(Switcher.ID);
        //endregion



        //region RecyclerSettings
        mRecyclerView = view.findViewById(R.id.recycler);
        LinearLayoutManager llm = new LinearLayoutManager(view.getContext());
        llm.setStackFromEnd(true);
        mRecyclerView.setLayoutManager(llm);
        mAdapterRec = new AdapterRec(dataset);
        mRecyclerView.setAdapter(mAdapterRec);
        mRecyclerView.smoothScrollToPosition(mAdapterRec.getItemCount());
        //endregion

        ispicked = false;


        //region View Settings+ OnClicks
        isPickedView = view.findViewById(R.id.is_picked);


        smileButton = view.findViewById(R.id.smile);

        mImageButton = view.findViewById(R.id.sendButton);
        mImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MessageModels models = new MessageModels();
                models.setText(mCompleteTextView.getText().toString());
                models.setFrom(CurrentUser.mId);
                models.setTo(Switcher.ID);
                if (WEB_URL.matcher(mCompleteTextView.getText().toString()).matches()) {
                    models.setURL(true);

                    parser = new HTMLParser(mCompleteTextView.getText().toString());
                }
                mAdapterRec.addMessage(models);

                dataset = mParsedMessages.createDataForSingleDialog(Switcher.ID);

                mAdapterRec = new AdapterRec(dataset);

                mRecyclerView.setAdapter(mAdapterRec);
                mRecyclerView.smoothScrollToPosition(mAdapterRec.getItemCount());

                mCompleteTextView.setText("");
                if (ispicked){
                    isPickedView.setVisibility(View.GONE);
                }

            }
        });



        mBackButton = view.findViewById(R.id.back_button);
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                Fragment fragment = new DialogListFragment();
                fm.beginTransaction().replace(R.id.container, fragment).commit();
            }
        });

        mAddFilesButton = view.findViewById(R.id.add_filesButton);
        mAddFilesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, PICK_IMAGE);
                isPickedView.setVisibility(View.VISIBLE);
                try {
                    Thread.sleep(200);
                    ispicked = true;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });
        //endregionss


        //region Smiles
        mCompleteTextView = view.findViewById(R.id.multiAutoCompleteTextView);
        EmojIconActions  emojIcon=new EmojIconActions(getContext(),view, mCompleteTextView,smileButton);
        emojIcon.ShowEmojIcon();
        //endregion


        return view;
    }


    //region Pick Images
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        if (resultCode==RESULT_OK){
            final Uri imageUri = imageReturnedIntent.getData();
            InputStream imageStream = null;
            try {
                imageStream = getActivity().getContentResolver().openInputStream(imageUri);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
        }

    }
    //endregion
}