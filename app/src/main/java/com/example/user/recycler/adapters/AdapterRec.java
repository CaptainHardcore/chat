package com.example.user.recycler.adapters;

import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.recycler.fragments.MainFrafment;
import com.example.user.recycler.network.HTMLParser;
import com.example.user.recycler.services.CurrentUser;
import com.example.user.recycler.message_models.MessageModels;
import com.example.user.recycler.message_models.MessagesSingleton;
import com.example.user.recycler.R;

import java.util.ArrayList;

/**
 * Created by user on 7/4/17.
 */

public class AdapterRec extends RecyclerView.Adapter<AdapterRec.ViewHolder> {

    private ArrayList<MessageModels> models;


    public AdapterRec(ArrayList<MessageModels> models) {
        this.models = models;
    }

    @Override
    public AdapterRec.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item2, parent, false);
        return new ViewHolder(view) ;

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
         boolean b =    models.get(position).isURL();


            if (models.get(position).getFrom().equals(CurrentUser.mId)) {

                holder.mCardViewNotMe.setVisibility(View.GONE);
                holder.mCardViewMe.setVisibility(View.VISIBLE);
                if (models.get(position).isURL()){

                    try {
                        holder.mCardViewMe.setText(MainFrafment.parser.getTitle());
                        holder.mCardViewMe.setTextColor(holder.mView.getResources().getColor(R.color.colorAccent, null));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
               else holder.mCardViewMe.setText(models.get(position).getText());
            } else {
                holder.mCardViewMe.setVisibility(View.GONE);
                holder.mCardViewNotMe.setVisibility(View.VISIBLE);

                holder.mTextNotMe.setText(models.get(position).getText());
                if (!models.get(position).isRead()) {
                    holder.mIsReadView.setVisibility(View.VISIBLE);
                }
            }


        }

    public  void addMessage(MessageModels model){
        MessagesSingleton.getInstance().models.add(model);
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return models.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        final View mView;
        final TextView mCardViewMe;
        final LinearLayout mCardViewNotMe;
        final ImageView mIsReadView;
        final TextView mTextNotMe;

        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
             mCardViewMe = itemView.findViewById(R.id.card_me);
            mCardViewNotMe = itemView.findViewById(R.id.card_not_me);
            mIsReadView = itemView.findViewById(R.id.is_read);
            mTextNotMe = itemView.findViewById(R.id.text_not_me);
        }
    }
}
