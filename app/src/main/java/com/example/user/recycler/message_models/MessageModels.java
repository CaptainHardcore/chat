package com.example.user.recycler.message_models;

/**
 * Created by user on 7/4/17.
 */

public class MessageModels {

    private boolean currentUser = false;
    private String text;
    private String name;
    private String from;
    private String to;
    private boolean online = false;
    private String date;
    private boolean read;
    private boolean isURL = false;





    //region Constructor
    public MessageModels() {
        this.currentUser = false;

    }
    //endregion

    //region Getters
    public boolean isRead() {
        return read;
    }
    public String getDate() {
        return date;
    }
    public boolean isOnline() {
        return online;
    }
    public boolean isCurrentUser() {
        return currentUser;
    }
    public String getText() {
        return text;
    }
    public String getName() {
        return name;
    }
    public String getTo() {
        return to;
    }
    public String getFrom() {
        return from;
    }
    public boolean isURL() {
        return isURL;
    }
    //endregion

    //region Setters
    public MessageModels setTo(String to) {
        this.to = to;
        return this;
    }
    public MessageModels setFrom(String from) {
        this.from = from;
        return this;
    }
    public MessageModels setCurrentUser(boolean currentUser) {
        this.currentUser = currentUser;
        return this;
    }
    public MessageModels setText(String text) {
        this.text = text;
        return this;
    }
    public MessageModels setName(String name) {
        this.name = name;
        return this;
    }
    public MessageModels setOnline(boolean online) {
        this.online = online;
        return this;
    }
    public MessageModels setDate(String date) {
        this.date = date;
        return this;
    }
    public MessageModels setRead(boolean read) {
        this.read = read;
        return this;
    }
    public MessageModels setURL(boolean URL) {
        isURL = URL;
        return this;
    }
    //endregion

}
