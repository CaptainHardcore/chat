package com.example.user.recycler.message_models;

import android.util.Log;

import com.example.user.recycler.services.CurrentUser;
import com.example.user.recycler.services.Switcher;

import java.util.ArrayList;

/**
 * Created by OLEG on 7/6/17.
 */

public class MessagesSingleton {
    private MessagesSingleton() {
        models = new ArrayList<>();
        //TODO Здесь мы должны получать данные в новом потоке
        //region Hardcode
        for (int i = 0; i <50 ; i++) {
            MessageModels msg = new MessageModels();
           if (i%3==0){
               msg.setTo("User");
           }
           else msg.setTo(CurrentUser.mId);
            if (i%4==0){
                msg.setFrom("Alex")
                .setText("This is Alez")
                .setOnline(true)
                .setRead(true);

            }
            else if (i%2==0){
                msg.setFrom("Max");
                msg.setText("this is max");
             }
             else if (i%5==0){
                 msg.setFrom("Kim");
                msg.setText("this is kim");

             }
             else {msg.setFrom("Sara");
             msg.setText("thids is sara");}
            msg.setOnline(true);
            msg.setRead(true);
            Log.i("Model", msg.getFrom()+" "+msg.getTo());
             models.add(msg);


        }
        for (int i = 0; i <50 ; i++) {
            MessageModels model = new MessageModels();
            model.setFrom("Nastya");
            model.setTo(CurrentUser.mId);
            model.setText("everybody Vants to be an Austronaut");
            models.add(model);
        }for (int i = 0; i <50 ; i++) {
            MessageModels model = new MessageModels();
            model.setFrom("Alexey");
            model.setTo(CurrentUser.mId);
            model.setText("i'm a hero");
            model.setOnline(true);
            models.add(model);

        }for (int i = 0; i <50 ; i++) {
            MessageModels model = new MessageModels();
            model.setFrom("Fecla");
            model.setTo(CurrentUser.mId);
            model.setText("Do you like Brodskiy");
            model.setOnline(true);
            models.add(model);

        }for (int i = 0; i <50 ; i++) {
            MessageModels model = new MessageModels();
            model.setFrom("Mahmud");
            model.setTo(CurrentUser.mId);
            model.setText("LEZGINKA AND SHASHLYK");
            models.add(model);
        }for (int i = 0; i <50 ; i++) {
            MessageModels model = new MessageModels();
            model.setFrom("GOsha");
            model.setTo(CurrentUser.mId);
            model.setText("i has a tonnely");
            model.setRead(true);
            models.add(model);
        }
        MessageModels m = new MessageModels();
        m.setFrom(CurrentUser.mId);
        m.setTo(Switcher.ID);
        m.setText("asd");
        models.add(m);
        //endregion
    }

    private static MessagesSingleton ourInstance;
   public  ArrayList<MessageModels> models;
    public static MessagesSingleton getInstance() {
        if (ourInstance==null){
            ourInstance = new MessagesSingleton();
        }
        return ourInstance;
    }


}
