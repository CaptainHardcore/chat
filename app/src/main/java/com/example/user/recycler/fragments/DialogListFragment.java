package com.example.user.recycler.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.user.recycler.message_models.MessagesParsed;
import com.example.user.recycler.R;
import com.example.user.recycler.adapters.DialogsListAdapter;
import com.example.user.recycler.services.Switcher;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by OLEG on 7/6/17.
 */

public class DialogListFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private DialogsListAdapter mDialogsListAdapter;
    private CircleImageView mCircleImageView;
    private TextView mToolbarTitleView;
    private MessagesParsed msgParsed;

    /**
     * работает только тут, на обьявление и инициализацию не разделяется
     *
     * onClickListener for CardView from Dialoglist
     * this method began trnsaction and replaced DialogFragment to MainFragment
     * and set to Switcher.ID TAG id messages recepient
     */

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.dia_card:
                    FragmentManager manager = getActivity().getSupportFragmentManager();
                    Fragment fragment = new MainFrafment();
                    Switcher.ID= view.findViewById(R.id.diaview).getTag().toString();
                            Log.i("From", Switcher.ID);
                    manager.beginTransaction().replace(R.id.container, fragment).commit();
                    break;
            }

        }
    };


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_list, container, false);
        //TODO желательно выполнять в новом потоке следующую строчку, получение массива сообщений
        msgParsed = new MessagesParsed();
        mDialogsListAdapter = new DialogsListAdapter(msgParsed.getUniqueId(),mOnClickListener );

        mRecyclerView = view.findViewById(R.id.recycler_dia);
        LinearLayoutManager llm2 = new LinearLayoutManager(view.getContext());
        mRecyclerView.setLayoutManager(llm2);
        mRecyclerView.setAdapter(mDialogsListAdapter);

        mCircleImageView = view.findViewById(R.id.toolbar_ava);
        mCircleImageView.setVisibility(View.GONE);
        mToolbarTitleView = view.findViewById(R.id.username_toolbar);
        mToolbarTitleView.setText("Dialogs");

        return view;
    }



}
