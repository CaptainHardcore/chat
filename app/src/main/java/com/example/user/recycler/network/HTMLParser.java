package com.example.user.recycler.network;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Created by OLEG on 7/13/17.
 */

public class HTMLParser  {

    Loader mLoader;
    String html;

    public HTMLParser(String link) {
        mLoader = new Loader(link);
    }
    public  String getTitle() throws InterruptedException {
        mLoader.t.join();
        Document doc = Jsoup.parse(mLoader.getHTML());
        Elements mElement = doc.getElementsByTag("title");
        return mElement.text();
    }



}