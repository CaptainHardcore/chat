package com.example.user.recycler.services;

/**
 * Created by user on 7/5/17.
 * this is a class of current user
 *
 */

public class CurrentUser {
    public static String mName;
    public static String mId = "Oleg";

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }




}
